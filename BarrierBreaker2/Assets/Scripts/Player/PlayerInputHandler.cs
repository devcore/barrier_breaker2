﻿/* Script to control player movement and display the forces in the UI. */
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class PlayerInputHandler : MonoBehaviour 
{
    public Image speed_bar;       // Image that fills to display player speed
    public Image x_input_bar;     // Image that fills to display x input amount
    public Image y_input_bar;     // Image that fills to display y input amount
    public Image delta_bar;       // Image that fills to display input delta amount
    public Image x_deadzone_mark; // Image that displays X axis deadzone
    public Image y_deadzone_mark; // Image that displays Y axis deadzone
    public Image teshold_mark;    // Image that displays velocity change treshold
    public Text speed_value;      // Text that displays player speed value
    public Text x_input_value;    // Text that displays player x input value
    public Text y_input_value;    // Text that displays player y input value
    public Text delta_value;      // Text that displays player delta input value
    public ParticleSystem hit_ps; // Velocity change particle system
    public AudioSource audio_src; // Velocity change SFX

    private Rigidbody m_rbody;           // Player rigid body
    private Vector3 m_avg_input;         // Previous user input (averaged)
    private float m_highest_delta;       // Highest recorded change in input
    private float m_sensitivity;         // Accelerometer sensitivity
    private float m_threshold;           // Velocity change treshold
    private float m_deadzone;          // Input deadzone for x and y axis
    private float m_speed = 15.0f;       // Player movement speed
    private float m_max_speed = 80.0f;   // Player maximum speed
    private bool m_freeze = true;        // Locks player character in place
    private bool m_zero_input = false;   // Zero input mode
    private bool m_freeze_input = false; // Allows player to control the character
    private bool m_break = false;        // Decreases player speed
    private Vector3 m_freeze_position;

    void Start() 
    {
        m_rbody = gameObject.GetComponent<Rigidbody>();
        // Load player input settings
        AdjustInputSettings();
        // Use raycast collision detection, since the speed is extreme
        m_rbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        m_avg_input = new Vector3(0, 0, 0);
        m_highest_delta = 0.0f;
        m_freeze_position = transform.position;
        // Make sure time scale is currect
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }

    void FixedUpdate()
    {
        if (m_freeze_input) { return; }
        // Read accelerometer value or controller input
        Vector3 input = new Vector3(Input.acceleration.x, 0.0f, Input.acceleration.y);
        // Increase input by user sensitivity
        input *= m_sensitivity;
//#if UNITY_EDITOR || UNITY_STANDALONE // Note(me): add full controller support
        // Controller input is just used for testing (similar values)
        input.x += Input.GetAxisRaw("Horizontal");
        input.z += Input.GetAxisRaw("Vertical");
//#endif
        // Limit linear input force
        input.x = Mathf.Clamp(input.x, -1.2f, 1.2f);
        input.z = Mathf.Clamp(input.z, -1.2f, 1.2f);
        // Update UI display
        x_input_bar.fillAmount = Mathf.Clamp(Mathf.Abs(input.x), 0.3f, 1.0f);
        y_input_bar.fillAmount = Mathf.Clamp(Mathf.Abs(input.z), 0.3f, 1.0f);
        x_input_value.text = input.x.ToString("0.0");
        y_input_value.text = input.z.ToString("0.0");
        // Stop if there is no input
        if (input.sqrMagnitude == 0)
        {
            // Keep averaging input
            m_avg_input *= 0.5f;
            // Update UI delta display
            delta_bar.fillAmount = Mathf.Clamp(m_highest_delta * 0.9f, 0.0f, 1.0f);
            delta_value.text = m_highest_delta.ToString("0.0");
            m_highest_delta *= 0.99f;
            return;
        }
        // Add impulse force if users whats to quickly change direction
        float delta_x = Mathf.Abs(input.x) - Mathf.Abs(m_avg_input.x);
        float delta_z = Mathf.Abs(input.z) - Mathf.Abs(m_avg_input.z);
        float total = delta_x + delta_z;
        // Save the highest change in input
        if (total > m_highest_delta)
        {
            m_highest_delta = total;
        }
        // Update UI delta display
        delta_bar.fillAmount = Mathf.Clamp(m_highest_delta, 0.0f, 1.0f);
        delta_value.text = m_highest_delta.ToString("0.0");
        m_highest_delta *= 0.99f;
        // Change trajectory if input changed drastically
        if (total > m_threshold)
        {
            // Compute velocity in new direction and reduce a bit!
            m_rbody.velocity = input.normalized * m_rbody.velocity.magnitude * 0.95f;
            // Change the average input to equal new direction
            m_avg_input = input;
            // Emit particle and play sound
            hit_ps.Emit(1);
            audio_src.Play();
            //Debug.Log("Total: " + total + " x: " + delta_x + " z: " + delta_z);
            //Debug.DrawRay(transform.position, input * 3.0f, Color.red, 0.3f);
            //Debug.DrawRay(transform.position, m_prev_input * 3.0f, Color.yellow, 0.3f);
        }
        // Store average input
        m_avg_input = (m_avg_input + input) * 0.5f;
        // Don't add force in zero input mode
        if (m_zero_input) { return; }
        // Course correction force
        float vel_length = m_rbody.velocity.magnitude;
        float delta_angle = Vector3.Dot(input, m_rbody.velocity) /
                                        (input.magnitude * vel_length);
        if (vel_length > 1.0f && delta_angle < 0.85f && delta_angle > -0.1f)
        {
            m_rbody.velocity *= 0.95f;
            m_rbody.velocity += input.normalized * vel_length * 0.06f;
        }
        // If the input is lower than deadzone remove it
        if (Mathf.Abs(input.x) < m_deadzone) { input.x = 0.0f; }
        if (Mathf.Abs(input.z) < m_deadzone) { input.z = 0.0f; }
        // Add force ignoring the objects mass
        m_rbody.AddForce(input * m_speed, ForceMode.Acceleration);
        // Apply break damping
        if (m_break) { m_rbody.velocity *= 0.95f; }
    }

    void LateUpdate()
    {
        if (m_freeze)
        {
            // Stop player
            m_rbody.velocity = new Vector3(0, 0, 0);
            m_rbody.position = m_freeze_position;
        }
        else
        {
            // Limit player movement speed
            m_rbody.velocity = Vector3.ClampMagnitude(m_rbody.velocity, m_max_speed);
        }
        // Update UI display
        float player_speed = m_rbody.velocity.magnitude;
        speed_bar.fillAmount = player_speed * 0.0125f; // Change to division if max speed changes
        speed_value.text = player_speed.ToString("0.00");
    }

    public void AdjustInputSettings()
    {
        // Use new input sensitivity settings
        m_sensitivity = PlayerPrefs.GetFloat("sensitivity", 3.0f);
        m_threshold = PlayerPrefs.GetFloat("threshold", 0.52f);
        m_deadzone = PlayerPrefs.GetFloat("deadzone", 0.3f);
        x_deadzone_mark.GetComponent<RectTransform>().anchoredPosition = new Vector3(m_deadzone * 200.0f, 0, 0);
        y_deadzone_mark.GetComponent<RectTransform>().anchoredPosition = new Vector3(m_deadzone * 200.0f, 0, 0);
        teshold_mark.GetComponent<RectTransform>().anchoredPosition = new Vector3(m_threshold * 200.0f, 0, 0);
    }

    public void FreezeCharacter(bool state)
    {
        // Locks player in place
        m_freeze = state;
        m_freeze_position = transform.position;
    }

    public void FreezeInput(bool state)
    {
        // Freeze input
        m_freeze_input = state;
        // Reset ui force display
        x_input_bar.fillAmount = 0.0f;
        y_input_bar.fillAmount = 0.3f;
        delta_bar.fillAmount = 0.0f;
        x_input_value.text = "0.0";
        y_input_value.text = "0.0";
        delta_value.text = "0.0";
    }

    public void ToggleZeroInput(bool state) { m_zero_input = state; }

    public void ToggleBreak(bool state) { m_break = state; }
}



// Note(core): Seems to make things worse... may test again later
/*private const float LowPassKernelWidthInSeconds = 1.0f;
private const float AccelerometerUpdateInterval = 1.0f / 60.0f;
private float LowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds; // tweakable
private Vector3 lowPassValue = new Vector3(0, 0, 0);*/
/*Vector3 LowPassFilterAccelerometer()
{
    lowPassValue = Vector3.Lerp(lowPassValue, Input.acceleration, LowPassFilterFactor);
    return lowPassValue;
}*/

/* OLD method
    // Dash: change velocity vector sharply
    void Dash(ref Vector3 input)
    {
        if (m_prev_input.sqrMagnitude == 0) { return; } 
        float delta_angle = Vector3.Dot(input, m_prev_input) / (input.magnitude * m_prev_input.magnitude);
        Debug.DrawRay(transform.position, input.normalized * 2.0f, new Color(0, 1, 0));
        Debug.DrawRay(transform.position, m_prev_input.normalized * 2.0f, new Color(1, 0, 0));

        if (delta_angle < -0.997f)
        {
            Vector3 dash_force = input.normalized * m_rbody.velocity.magnitude * 2.0f;
            m_rbody.AddForce(dash_force, ForceMode.VelocityChange);
            Debug.Log("~180DegTurn");
        }
        else if (delta_angle < 0.7f)
        {
            Vector3 dash_force = input.normalized * m_rbody.velocity.magnitude * 2.0f;
            m_rbody.AddForce(dash_force, ForceMode.VelocityChange);
            Debug.Log("~90DegTurn");
        }
    
 * }*/
﻿// Orbits main texture offset
using UnityEngine;

public class TexturePanEffect : MonoBehaviour
{
    public float distance = 0.2f;     // Pan Offset
    public float frequency = 3.0f;    // Wave frequency
    public float amplitude = 0.015f;  // Wave height
    public float speed = 0.3f;        // Pan speed

    private Material m_mat;
    private Vector2 orbit;

    void OnEnable()
    {
        GetComponent<ParticleSystem>().Emit(1);
    }

	void Start () 
    {
        m_mat = GetComponent<Renderer>().material;
	}

	void Update() 
    {
        float dt = Time.time * speed;
        // Wave height
        float wave = distance + Mathf.Sin(dt * frequency) * amplitude;
        // Orbit position
        orbit.x = 0.5f + Mathf.Cos(dt) * wave;
        orbit.y = 0.5f + Mathf.Sin(dt) * wave;
        m_mat.mainTextureOffset = orbit;
    }
}

﻿/* Controls barrier status and particle effects */
using UnityEngine;

public class BarrierHandler : MonoBehaviour 
{
    public ParticleSystem base_particles;
    public ParticleSystem edge_particles;
    public ParticleSystem hit_wave;
    public ParticleSystem hit_fract;
    public AudioSource hit_rebound;
    public AudioSource hit_damage;
    public int health = 100;

    void ReceiveDamage(Rigidbody player_rb)
    {
        // Take damage only if the player hits it with a velocity of 70 or higher
        float player_speed = player_rb.velocity.magnitude;
        if (player_speed > 50.0f)
        {
            hit_wave.Emit(1);
            hit_damage.Play();
            // Slow down time to emphasize hit
            Time.timeScale = 0.1f;
            Time.fixedDeltaTime = 0.002f; // 0.02 * 0.1;
            Invoke("RevokeSlowdown", 0.15f);
            // Increase the number of fracture particles
            hit_fract.maxParticles += 50;
            // Clamp speed after damage
            player_rb.velocity = Vector3.ClampMagnitude(player_rb.velocity, 2.0f);
            if (hit_fract.maxParticles >= health)
            {
                // Disable particle system to form an explotion
                ExplodeBarrier();
                // Disable collider
                GetComponent<Collider>().enabled = false;
                // Delare that level was cleared
                GameManager.instance.LevelCleared();
            }
        } 
        else 
        {
            hit_rebound.Play();
            // Clamp speed after bounce
            player_rb.velocity = Vector3.ClampMagnitude(player_rb.velocity, 15.0f);
        }
    }

    void ExplodeBarrier()
    {
        // Slowdown time less then after a damaging hit
        Time.timeScale = 0.25f;
        Time.fixedDeltaTime = 0.005f;
        // Destroy existing base, edge particles and others after a longer delay. 
        Destroy(base_particles, 0.5f);
        Destroy(edge_particles, 0.1f);
        Destroy(hit_fract, 1.0f);
        // Loop through all the _fracture_ particles and give them a random colour
        // and velocity towards the center of the system.
        Color rng_color;
        ParticleSystem.Particle[] m_particles = new ParticleSystem.Particle[hit_fract.maxParticles];
        int m_num_particles = hit_fract.GetParticles(m_particles);
        for (int i = 0; i < m_num_particles; i++)
        {
            // Random colour, bigger size and extend lifetime.
            rng_color.r = Random.Range(0.0f, 1.0f);
            rng_color.g = Random.Range(0.0f, 1.0f);
            rng_color.b = Random.Range(0.0f, 1.0f);
            rng_color.a = 1.0f;
            m_particles[i].startColor = rng_color;
            m_particles[i].startSize = 16.0f;
            m_particles[i].lifetime = 0.5f;
            // Make particles go towards the center of the system
            Vector3 dir = hit_fract.transform.position - m_particles[i].position;
            m_particles[i].velocity = dir.normalized * 10.0f;
        }
        // Apply changes
        hit_fract.SetParticles(m_particles, m_num_particles);
        hit_fract.Stop();
    }

    void RevokeSlowdown()
    {
        hit_wave.Emit(1);
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.02f;
    }
}


/*private bool m_slowdown = false;
void FixedUpdate() 
{
    if (m_slowdown)
    {
        Debug.Log(Time.timeScale);
        if (Time.timeScale < 1.0f)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1.1f, Time.deltaTime*4.0f);
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
        }
        else
        {
            m_slowdown = false;
            Time.timeScale = 1.0f;
            Time.fixedDeltaTime = 0.02f;
        }
    }
}*/
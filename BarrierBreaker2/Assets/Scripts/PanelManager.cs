﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class PanelManager : MonoBehaviour 
{
	public Animator init_open_anim;

	private Animator m_open_anim;
	//private GameObject m_prev_selected;
	private int m_open_id;
    private int m_close_id;

	public void OnEnable()
	{
        m_open_id = Animator.StringToHash("Open");
        m_close_id = Animator.StringToHash("Closed");
        // Open panel first time
        if (init_open_anim != null)
        { 
		    OpenPanel(init_open_anim);
        }
	}

	public void OpenPanel (Animator anim)
	{
        // Check is panel is already open
        if (m_open_anim == anim) { return; }
		// Set panel to active
		anim.gameObject.SetActive(true);
		//anim.transform.SetAsLastSibling();
        // Save currently selected and close it
        //m_prev_selected = EventSystem.current.currentSelectedGameObject;
		CloseCurrent();
        // Change to new menu
		m_open_anim = anim;
		m_open_anim.SetBool(m_open_id, true);
        // Select first option in new menu
        //EventSystem.current.SetSelectedGameObject(FirstEnabledSelectable(anim.gameObject));
        EventSystem.current.SetSelectedGameObject(null);
	}

	static GameObject FirstEnabledSelectable(GameObject gameObject)
	{
		var selectables = gameObject.GetComponentsInChildren<Selectable>(true);
        for (int i = 0; i < selectables.Length; i++)
        {
            if (selectables[i].IsActive() && selectables[i].IsInteractable())
            {
                return selectables[i].gameObject;
            }
        }
		return null;
	}

	public void CloseCurrent()
	{
        if (m_open_anim == null) { return; }
        // Disable current state, start a coroutine and set selected
		m_open_anim.SetBool(m_open_id, false);
        //EventSystem.current.SetSelectedGameObject(m_prev_selected);
		StartCoroutine(DisablePanelDeleyed(m_open_anim));
		m_open_anim = null;
	}
	IEnumerator DisablePanelDeleyed(Animator anim)
	{
        // Loop waiting a frame until animation finishes, then disable object.
		bool closed_state_reached = false;
		bool want_to_close = true;
		while (!closed_state_reached && want_to_close)
		{
            if (!anim.IsInTransition(0)) 
            {
                closed_state_reached = m_close_id == 
                                       anim.GetCurrentAnimatorStateInfo(0).shortNameHash;
            }
			want_to_close = !anim.GetBool(m_open_id);
			yield return new WaitForEndOfFrame();
		}
        if (want_to_close)
        { 
			anim.gameObject.SetActive(false);
        }
	}
}

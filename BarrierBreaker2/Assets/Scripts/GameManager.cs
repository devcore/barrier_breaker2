﻿/* Persistant object the uses the singleton pattern to control flow between scenes. */
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
    public static GameManager instance = null;
    public LevelManager level_manager = null;   // Each level has a level manager for state and data flow
    public bool level_restarted = false;        // Flag to see if level has been restarted
    public float clear_time = 0.0f;             // Time taken to clear the game from level 0
    
    void Awake()
    {
        // Create if instance does not exist
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            // Do not allow more instances to be created
            Destroy(gameObject);
        }
        // Do not destroy object in between scenes
        DontDestroyOnLoad(this);
        // Do not sleep in this app
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void SceneChanged()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }

    public void StartCurrentLevel()
    {
        level_manager.StartLevel();
    }

    public void LevelCleared()
    {
        level_manager.LevelCleared();
    }

    public void LevelFailed()
    {
        level_manager.LevelFailed();
    }

    public void GameCleared()
    {
        float prev_clear_time = PlayerPrefs.GetFloat("clear_time", 999.9f);
        if (clear_time < prev_clear_time)
        {
            PlayerPrefs.SetFloat("clear_time", clear_time);
        }
    }

	public void Quit() 
	{
		#if UNITY_EDITOR
		    UnityEditor.EditorApplication.isPlaying = false;
		#else
		    Application.Quit();
		#endif
	}
}

﻿/* Used to calibrate input sensitivity and velocity change threshold. */
using UnityEngine;
using UnityEngine.UI;

public class InputCalibration : MonoBehaviour 
{
    public PlayerInputHandler m_input_handler;  // Script that controls player character
    public Slider sens_slider;      // Controls sensitivity value
    public Slider deadzone_slider;  // Controls deadzone value
    public Slider treshold_slider;  // Controls threshold value
    public Text sens_label;         // Displays sensitivity value
    public Text deadzone_label;     // Displays deadzone value
    public Text treshold_label;     // Displays threshold value

	void Start() 
    {
        float current_sens = PlayerPrefs.GetFloat("sensitivity", 3.0f);
        float current_deadzone = PlayerPrefs.GetFloat("deadzone", 0.3f);
        float current_thresh = PlayerPrefs.GetFloat("threshold", 0.52f);

        sens_slider.value = current_sens;
        deadzone_slider.value = current_deadzone;
        treshold_slider.value = current_thresh;
        sens_label.text = current_sens.ToString("0.0");
        deadzone_label.text = current_deadzone.ToString("0.00");
        treshold_label.text = current_thresh.ToString("0.00");

	}

    public void NewInputSettings()
    {
        PlayerPrefs.SetFloat("sensitivity", sens_slider.value);
        PlayerPrefs.SetFloat("deadzone", deadzone_slider.value);
        PlayerPrefs.SetFloat("threshold", treshold_slider.value);
        sens_label.text = PlayerPrefs.GetFloat("sensitivity", 3.0f).ToString("0.0");
        deadzone_label.text = PlayerPrefs.GetFloat("deadzone", 0.3f).ToString("0.00");
        treshold_label.text = PlayerPrefs.GetFloat("threshold", 0.52f).ToString("0.00");
        m_input_handler.AdjustInputSettings();
    }
}

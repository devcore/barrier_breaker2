﻿using UnityEngine;
using UnityEngine.UI;

public class CreditsManager : MonoBehaviour 
{
    public Text best_clear_label;
    public Text current_clear_label;

    void Start()
    {
        GameManager.instance.GameCleared();
        best_clear_label.text = PlayerPrefs.GetFloat("clear_time", 999.9f).ToString("0.00");
        current_clear_label.text = GameManager.instance.clear_time.ToString("0.00");
    }
}

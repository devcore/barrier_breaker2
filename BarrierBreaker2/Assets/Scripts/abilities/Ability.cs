﻿/* Base ability class */
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public string title;
    public Color color;
    public Sprite sprite;
    public AudioClip sound;
    public float duration = 0;
    public float cooldown = 1.0f;
    // Called to create an ability for a player
    public abstract void Initialise(GameObject go);
    // Called when an ability is used
    public abstract void TriggerAbility();
    // Called after ability duration expires (not called if duration is 0)
    public abstract void EndAbility();
}
﻿/* Scriptable bullet time ability:
 * Slows down time until duration expires*/
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/BulletTime")]
public class Ability_BulletTime : Ability
{
    [Range(0.1f, 1.0f)]  public float time_scale; // Effects duration

    public override void Initialise(GameObject go) { }

    public override void TriggerAbility()
    {
        // Start bullet time
        Time.timeScale = time_scale;
        Time.fixedDeltaTime = 0.02f * time_scale;
    }

    public override void EndAbility()
    {
        // Disable bullet time
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }
}

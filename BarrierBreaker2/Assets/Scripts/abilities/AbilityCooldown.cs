﻿/* Scriptable ability holder, which hooks abilities to monobehaviour callbacks:
 * Creates abilities for ui, triggers them and is able to swap for new ability. */
using UnityEngine;
using UnityEngine.UI;

public class AbilityCooldown : MonoBehaviour 
{
    public string ability_button = "Fire1";
    public Image cd_mask;        // Dark overlay used to indicate cooldown
    public Text cd_label;        // Displays cooldown time remaining
    public Text ability_title;   // Name of the ability
    public Ability m_ability;    // Assigned ability
    public GameObject m_player;

    private Image m_image;          // Displays ability image
    private AudioSource m_sound;    // Plays ability sound
    private float m_cd_ready_time;  // Point in time ability is off cooldown
    private float m_cd_remaining;   // Amount of cooldown time left
    private bool m_cd_complete = true;
    
    void Start()
    {
        if (m_player == null)
        { 
            m_player = GameManager.instance.level_manager.player;
        }
        m_image = GetComponent<Image>();
        m_sound = GetComponent<AudioSource>();
        m_image.sprite = m_ability.sprite;
        m_image.color = m_ability.color;
        cd_mask.sprite = m_ability.sprite;
        m_sound.clip = m_ability.sound;
        ability_title.text = m_ability.title;
        m_ability.Initialise(m_player);
        AbilityReady();
	}

	void Update()
    {
        // If cooldown is complete set ability to ready and wait for input
        m_cd_complete = (Time.time > m_cd_ready_time);
        if (m_cd_complete)
        {
            AbilityReady();
            if (Input.GetButtonDown(ability_button))
            {
                ButtonTriggered();
            }
        }
        else
        {
            // Decrease cooldown
            m_cd_remaining -= Time.deltaTime;
            cd_label.text = m_cd_remaining.ToString("0.0");
            cd_mask.fillAmount = m_cd_remaining / m_ability.cooldown;
        }
	}

    public void ButtonTriggered()
    {
        // Exit if cooldown is incomplete (from other source)
        if (!m_cd_complete) { return; }
        // Start cooldown time
        m_cd_ready_time = m_ability.cooldown + Time.time;
        m_cd_remaining = m_ability.cooldown;
        cd_label.enabled = true;
        cd_mask.enabled = true;
        // Execute ability and play sound
        m_ability.TriggerAbility();
        m_sound.Play();
        // Set hook for to execute after ability duration
        if (m_ability.duration > Mathf.Epsilon)
        {
            Invoke("RevokeEndAbility", m_ability.duration);
        }
    }

    public void SwapAbility(Ability new_ability)
    {
        // Setup new ability
        m_ability = null;
        m_ability = new_ability;
        m_image.sprite = m_ability.sprite;
        m_image.color = m_ability.color;
        cd_mask.sprite = m_ability.sprite;
        m_sound.clip = m_ability.sound;
        ability_title.text = m_ability.title;
        m_ability.Initialise(m_player);
        AbilityReady();
        m_cd_ready_time = 0.0f;
        m_cd_remaining = 0.0f;
    }

    private void AbilityReady()
    {
        cd_label.enabled = false;
        cd_mask.enabled = false;
    }

    private void RevokeEndAbility()
    {
        m_ability.EndAbility();
    }
}

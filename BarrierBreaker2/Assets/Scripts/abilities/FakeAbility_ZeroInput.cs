﻿// Allows player to zero out input
using UnityEngine;

public class FakeAbility_ZeroInput : MonoBehaviour 
{
    public PlayerInputHandler m_player_input;
	void Start () 
    {
        // Get player input handler
        if (m_player_input == null) 
        {
            m_player_input = GameManager.instance.level_manager.player.GetComponent<PlayerInputHandler>();    
        }
	}

    // Button is up, unfreeze player input
    public void Ability_ZeroInput(bool state) 
    {
        m_player_input.ToggleZeroInput(state); 
    }
}

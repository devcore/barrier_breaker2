﻿// Allows player to reduce their speed by holding a button
using UnityEngine;

public class FakeAbility_Break: MonoBehaviour 
{
    public PlayerInputHandler m_player_input;
	void Start () 
    {
        // Get player input handler
        if (m_player_input == null) 
        {
            m_player_input = GameManager.instance.level_manager.player.GetComponent<PlayerInputHandler>();
        }
	}

    // Button is up, unfreeze player input
    public void Ability_Break(bool state) 
    { 
        m_player_input.ToggleBreak(state); 
    }
}

﻿/* Scriptable boost ability:
 * Boost player forward using boost force value. */
using UnityEngine;

[CreateAssetMenu(menuName="Abilities/Boost")]
public class Ability_Boost : Ability
{
    public float boost_force = 20.0f;
    private Rigidbody m_rbody;

    public override void Initialise(GameObject go)
    {
        m_rbody = go.GetComponent<Rigidbody>();
    }

    public override void TriggerAbility()
    {
        m_rbody.velocity += m_rbody.velocity.normalized * boost_force;
    }

    public override void EndAbility() { /* Empty */ }
}

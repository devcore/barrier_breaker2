﻿using UnityEngine;

public class PowerUpAbility : MonoBehaviour 
{
    public Ability ability;
    public AbilityCooldown ability_slot;

	void Start()
    {
        Material mat = GetComponent<Renderer>().material;
        // Get texture from sprite (note must not be packed in atlas,
        // since they are read only.
        mat.mainTexture = ability.sprite.texture;
        // Change colour to match ability level
        mat.SetColor("_TintColor", ability.color);
	}

    void UpgradePlayerAbility()
    {
        ability_slot.SwapAbility(ability);
        Destroy(gameObject);
    }
}

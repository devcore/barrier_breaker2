﻿/* Manages flow of current scene and its data */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour 
{
    public int first_level_id;
    public Animator level_start_anim;
    public GameObject win_menu;
    public GameObject lose_menu;
    public GameObject player;
    public GameObject player_ui;
    public Text time_label;
    public Text best_time_label;
    public Text current_time_label;
    
    private float level_time;
    private string level_name;
    private bool level_started = false;
    private Vector3 level_start_pos;

    void OnEnable()
    {
        GameManager.instance.level_manager = this;
        player_ui.gameObject.SetActive(false);
        level_name = "time_level_" + SceneManager.GetActiveScene().buildIndex;
        // Save camera start position and set the default height
        level_start_pos = Camera.main.transform.position;
        level_start_pos.y = 32.0f;
        // Play start animation of the level has one and was not restarted
        if (level_start_anim && !GameManager.instance.level_restarted)
        {
            player.SendMessage("FreezeInput", true);
            player.SendMessage("FreezeCharacter", true);
            level_start_anim.Play("StartAnimation");
        }
        else
        {
            level_start_anim.enabled = false;
            GameManager.instance.StartCurrentLevel();
        }
    }

    public void StartLevel()
    {
        // Initialise defaults
        level_started = true;
        level_time = 0.0f;
        time_label.text = level_time.ToString("0.00");
        // Give player control and show the user interface
        player.SendMessage("FreezeInput", false);
        player.SendMessage("FreezeCharacter", false);
        player_ui.gameObject.SetActive(true);
        // Reset clear time if the player restarts the session
        if(SceneManager.GetActiveScene().buildIndex == first_level_id)
        {
            GameManager.instance.clear_time = 0.0f;
        }
        // Make sure camera is in the right position
        Camera.main.transform.position = level_start_pos;
    }

    void Update()
    {
        // Return if level has not started
        if (!level_started) 
        {
            if (Input.GetButtonDown("Fire1"))
            {
                // Seems to be a better solution, since disabling the animator
                // does not allow obect to move until next frame...
                level_start_anim.speed = 1000.0f;
            }
            return;
        }
        if (Input.GetMouseButtonDown(1))
        {
            //Application.CaptureScreenshot("level_05_overview.png", 4);
        }
        // Increament timer
        level_time += Time.deltaTime;
        time_label.text = level_time.ToString("0.00");
    }

    public void LevelCleared()
    {
        level_started = false;
        // Display win menu
        win_menu.SetActive(true);
        win_menu.GetComponent<Animator>().SetBool("open", true);
        // Add level time to clear time
        GameManager.instance.clear_time += level_time;
        // If record if fastest run
        if(level_time < PlayerPrefs.GetFloat(level_name, 999.99f))
        {
            PlayerPrefs.SetFloat(level_name, level_time);
        }
        // Stop player character
        player.SendMessage("FreezeInput", true);
        player.SendMessage("FreezeCharacter", true);
        // Update labels
        best_time_label.text = PlayerPrefs.GetFloat(level_name, 999.99f).ToString("0.00");
        current_time_label.text = level_time.ToString("0.00");
    }

    public void LevelFailed()
    {
        level_started = false;
        // Display lose menu
        lose_menu.SetActive(true);
        lose_menu.GetComponent<Animator>().SetBool("open", true);
        // Add level time to clear time
        GameManager.instance.clear_time += level_time;
        // Do not allow further input
        player.SendMessage("FreezeInput", true);
        player.GetComponent<AudioSource>().Play();
        // Make sure player goes down!
        player.GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, -100.0f, 0.0f), ForceMode.Impulse);
    }

    void OnDestroy()
    {
        GameManager.instance.level_manager = null;
    }
}

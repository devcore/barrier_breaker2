﻿/* Generic platform properties:
 * Colour, glow, hit particles systems and hit sound. */
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlatformProperties : MonoBehaviour
{
    public Color colour;                 // Glow colour
    public ParticleSystem glow_particle; // Glow effect
    public ParticleSystem hit_particles; // Hit expanding glow
    
    private AudioSource m_audio_source;  // Plays collision sound

    void Start()
    {
        m_audio_source = GetComponent<AudioSource>();
        // Set particles to use the colour of the platform
        glow_particle.startColor = colour;
        hit_particles.startColor = colour;
        // Rotate to face the same way as platforms
        glow_particle.startRotation = transform.rotation.eulerAngles.y * Mathf.Deg2Rad;
        hit_particles.startRotation = transform.rotation.eulerAngles.y * Mathf.Deg2Rad;
        // Start the fake _glow_ effect
        glow_particle.Play();
    }

    // Called by using a message from the collider
    void PlaformEffects()
    {
        hit_particles.Emit(1);
        m_audio_source.Play();
    }
}

// Old stuff that worked with post processing which did not with mobile... 18 fps on nexus7

// store shared material to later re-use
//private Material m_shared_mat;
//private Material m_mat;

// Store shared material to avoid instantiating many
//m_shared_mat = GetComponent<Renderer>().sharedMaterial;
// Retrieve uniform id
//m_glow_id = Shader.PropertyToID("_MKGlowPower");
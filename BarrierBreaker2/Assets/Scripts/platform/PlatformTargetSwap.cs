﻿/* Swaps between two targets after duration repeatedly */
using UnityEngine;
using System;

[RequireComponent(typeof(PlatformReflect))]
public class PlatformTargetSwap : MonoBehaviour 
{
    public Transform[] targets;
    public float duration = 5.0f;

    private PlatformReflect m_platform;
    private bool m_target_id;
	
	void Start () 
    {
        m_platform = GetComponent<PlatformReflect>();
        Invoke("TargetSwap", duration);
	}

    void TargetSwap()
    {
        m_target_id = !m_target_id;
        m_platform.LookAtTarget(targets[Convert.ToInt32(m_target_id)]);
        Invoke("TargetSwap", duration);
    }
}

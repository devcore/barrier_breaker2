﻿/* Platform Reflect:
 * Reflects player towards target location. */
using UnityEngine;

public class PlatformReflect : MonoBehaviour 
{
    public GameObject guide_lines;          // Guide line particles
    public Transform target;                // Taget position
    public float reflect_multiplier = 0.8f; // Reflect force scaler

    private ParticleSystem m_particles;

    void Start()
    {
        m_particles = guide_lines.GetComponent<ParticleSystem>();
        LookAtTarget(target);
    }

    void Reflect(Rigidbody rb)
    {
        // Repel player towards target position
        Vector3 dir = target.position - rb.position;
        Vector3 ref_force = dir.normalized * rb.velocity.magnitude;
        rb.velocity *= Mathf.Clamp(1.0f-reflect_multiplier, 0.0f, 1.0f);
        rb.velocity += ref_force * reflect_multiplier;
    }

    public void LookAtTarget(Transform t)
    {
        // Rotate towards target direction
        guide_lines.transform.LookAt(t);
        // Calculate the duration particles need to live to reach target
        Vector3 dist = guide_lines.transform.position - t.position;
        m_particles.startLifetime = dist.magnitude / (m_particles.startSpeed + Mathf.Epsilon);
    }
}

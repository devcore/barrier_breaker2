﻿/* Loads menu scenes by id and levels by relativity */
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnEnter : StateMachineBehaviour 
{
    public int scene_id = 0; // Scene id to load

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        SceneManager.LoadScene(scene_id);
        GameManager.instance.level_restarted = false;
	}
}

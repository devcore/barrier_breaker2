﻿/* Loads menu scenes by id and levels by relativity */
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextOnEnter : StateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        GameManager.instance.level_restarted = false;
	}
}

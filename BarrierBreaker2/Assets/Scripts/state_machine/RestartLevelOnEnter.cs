﻿/* Reloads current level and sets reload state */
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevelOnEnter : StateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        GameManager.instance.level_restarted = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}

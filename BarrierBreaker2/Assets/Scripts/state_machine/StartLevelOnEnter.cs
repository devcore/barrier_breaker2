﻿/* Starts current level and stop the animator*/
using UnityEngine;

public class StartLevelOnEnter : StateMachineBehaviour 
{
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.enabled = false;
        GameManager.instance.StartCurrentLevel();
	}
}

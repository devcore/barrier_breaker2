﻿/* I just want an infinite particle... */
using UnityEngine;

public class EmitOnce : MonoBehaviour 
{
    void Awake()
    {
        GetComponent<ParticleSystem>().Emit(1);
        Destroy(this);
    }
}

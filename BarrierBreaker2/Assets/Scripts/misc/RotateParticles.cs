﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class RotateParticles : MonoBehaviour 
{
    public float rotation_speed = 5.0f;
    public Vector3 axis = new Vector3(0, 0, 1);

    private ParticleSystem m_particle_system;
    private ParticleSystem.Particle[] m_particles;

    void Start()
    {
        m_particle_system = GetComponent<ParticleSystem>();
        m_particles = new ParticleSystem.Particle[m_particle_system.maxParticles]; 
    }

	void FixedUpdate ()
    {
        int m_num_particles = m_particle_system.GetParticles(m_particles);
        for (int i = 0; i < m_num_particles; i++)
        {
            m_particles[i].angularVelocity3D = new Vector3(5.0f, 5.0f, 5.0f);
        }
        m_particle_system.SetParticles(m_particles, m_num_particles);
	}
}

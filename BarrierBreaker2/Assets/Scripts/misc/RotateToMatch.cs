﻿// Rotates particle system to match object direction
using UnityEngine;

public class RotateToMatch : MonoBehaviour 
{
	void Start() 
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        ps.startRotation = transform.rotation.eulerAngles.y * Mathf.Deg2Rad;
	}
}

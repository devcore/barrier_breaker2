﻿// Saves a screenshot
using UnityEngine;

public class TakeScreenShot : MonoBehaviour 
{
    public string file_name = "screenshot";
    private int num = 0;

    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            string filename = file_name + "_" + num + ".png";
            Debug.Log(filename + " taken");
            Application.CaptureScreenshot(filename, 4);
            num++;
        }
    }
}

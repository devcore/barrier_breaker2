﻿using UnityEngine;

public class EnableOnTrigger : MonoBehaviour 
{
    public GameObject go;

    public void ExecuteTrigger()
    {
        go.SetActive(true);
        Destroy(this.gameObject);
    }
}

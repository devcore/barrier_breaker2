﻿using UnityEngine;

public class OrbitPoint : MonoBehaviour 
{
    public float distance = 1.0f;     // Orbit Offset
    public float frequency = 3.0f;    // Wave frequency
    public float amplitude = 0.5f;    // Wave height
    public float speed = 5.0f;        // Orbit speed
    public float start_angle = 0.0f;
    public GameObject[] game_objects;

    void Update()
    {
        Vector3 orbit;
        float dt = Time.time * speed + start_angle;
        // Wave height
        float wave = distance + Mathf.Sin(dt * frequency) * amplitude;
        // Orbit position
        orbit.x = Mathf.Cos(dt) * wave;
        orbit.y = 0.0f;
        orbit.z = Mathf.Sin(dt) * wave;
        // Update object in group
        for (int i = 0; i < game_objects.Length; i++)
        {
            game_objects[i].transform.position = transform.position + orbit;
        }
    }
}

﻿/* Follows player using a spring force principle with extra methods. */
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerFollow : MonoBehaviour
{
    public Rigidbody rb_player;             // Player rigid body
    public float rest_distance = 5.0f;      // Distance at which the camera starts moving
    public float m_rotation_speed = 10.0f;  // Camera rotation speed scaler
    public float m_centering_speed = 0.5f;  // Speed at which the camera centers to player
    //public float m_elavation_speed = 0.2f;  // Speed at which the camera rises

    private Rigidbody m_rbody;            // Camera rigid body
    private Quaternion m_local_identity;  // Local copy of identity matrix
    private float m_rotation_x;           // Current rotation of x axis
    private float m_rotation_y;           // Current rotation of y axis
    //private float m_cam_height;           // Current camera height

    void Start()
    {
        // Square rest distance to avoid sq_root
        rest_distance *= rest_distance;
        // Default starting rotation
        m_rotation_x = 90.0f;
        m_rotation_y = 0.0f;
        m_local_identity = Quaternion.identity;
        m_rbody = GetComponent<Rigidbody>();
        // Camera shouldn't be able to collide with anything
        m_rbody.detectCollisions = false;
        transform.position = new Vector3(rb_player.position.x,
                                         transform.position.y, 
                                         rb_player.position.z);
        //m_cam_height = transform.position.y;
    }

    void LateUpdate()
    {
        // Calculate distance between player and camera ignoring height
        Vector3 dist = new Vector3(rb_player.position.x - transform.position.x, 0.0f,
                                   rb_player.position.z - transform.position.z);
        float strecth_dist = dist.sqrMagnitude - rest_distance - Mathf.Epsilon;
        float player_speed = rb_player.velocity.magnitude;
        // Move if stretch is greater than rest distance
        if (strecth_dist > rest_distance)
        {
            // Move along with player
            m_rbody.velocity = new Vector3(rb_player.velocity.x, 0.0f, rb_player.velocity.z);
            // Rotate towards movement direction
            Vector3 rot = dist.normalized * m_rotation_speed * Time.deltaTime;
            // Clamp rotation
            m_rotation_x = Mathf.Clamp(m_rotation_x - rot.z, 85, 95);
            m_rotation_y = Mathf.Clamp(m_rotation_y + rot.x, -5, 5);
            // If the player is moving slowly correct the camera
            if (player_speed < 15.0f)
            {
                Vector3 force = dist.normalized * strecth_dist * Time.fixedDeltaTime;
                m_rbody.velocity += force * m_centering_speed;
            }
            // Fix if camera gets bugged
            if (strecth_dist > 300.0f)
            {
                Vector3 force = dist.normalized * strecth_dist * Time.fixedDeltaTime;
                m_rbody.velocity += force * m_centering_speed * 4.0f;
            }
        }
        else
        {
            // Super damping
            m_rbody.velocity *= 0.9f;
        }
        // Change camera height based on player speed
        /*Vector3 new_pos = m_rbody.position;
        if (player_speed < 10.0f)
        {
            float speed = m_elavation_speed * Time.fixedDeltaTime;
            new_pos.y = Mathf.Clamp(new_pos.y - speed, 30.0f, 35.0f);
            m_rbody.position = new_pos;
        }
        else
        {
            float speed = (player_speed + m_elavation_speed) * Time.fixedDeltaTime;
            new_pos.y = Mathf.Clamp(new_pos.y + speed, 30.0f, 35.0f);
            m_rbody.position = new_pos;
        }*/
        // Update rotation
        transform.rotation = m_local_identity;
        transform.Rotate(transform.right, m_rotation_x, Space.World);
        transform.Rotate(transform.up, m_rotation_y, Space.World);
    }
}

﻿/* Script to determine what happens when the player collides with a plaform:
 * All platforms: Emit particles and play collision sound.
 * Platform Bounce: bounces player with additional force. 
 * Platform Reflect: bounces player towards target location.
 * Platform Slowmo: after collision slows down time after which boosts player towards input direction. 
 * Platform Stop: after collision stops player. */
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ParticleSystem))]
public class PlayerCollisionHandler : MonoBehaviour 
{
    public ParticleSystem[] ball_particles;  // Plasma ball particles that change
    public ParticleSystem hit_particles;     // Particles that emit when platform is hit
    
    private Rigidbody m_rbody;     // Player's rigid body
    private Color[] ball_colours;  // Original plama ball colours
    private Material m_trail_mat;  // Trail material
    private int m_trail_tint_id;   // Trail colour property id
    private bool m_bullet_time;    // Time scale is altered
    private float m_bullet_speed;  // Stores players speed they enter bullet time
    private const float m_colour_speed = 0.4f; // Rate at which colours transition

	void Start () 
    {
        m_rbody = GetComponent<Rigidbody>();
        m_trail_mat = GetComponentInChildren<TrailRenderer>().material;
        m_trail_tint_id = Shader.PropertyToID("_TintColor");
        ball_colours = new Color[ball_particles.Length];
        for (int i = 0; i < ball_particles.Length; i++)
        {
            ball_colours[i] = ball_particles[i].startColor;
        }
        m_bullet_time = false;
	}

    void FixedUpdate()
    {
        float lerp_amount = Time.deltaTime * m_colour_speed;
        // Slerp to original colour
        Color main_colour = Colorx.Slerp(ball_particles[0].startColor,
                                         ball_colours[0], lerp_amount);
        // Main particle colour
        main_colour.a = ball_colours[0].a;
        ball_particles[0].startColor = main_colour;
        // Trail particles
        main_colour.a = 1.0f;
        m_trail_mat.SetColor(m_trail_tint_id, main_colour);
        // Other particles
        for (int i = 1; i < ball_particles.Length; i++)
        { 
            ball_particles[i].startColor = Color.Lerp(ball_particles[i].startColor,
                                                      ball_colours[i], lerp_amount);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject go = collision.gameObject;
        if (go.CompareTag("platform_basic")) {
            // Generic bounce
            GenericPlatformHit(go, collision);
        } else if (go.CompareTag("platform_bounce")) {
            // Increased bounce by 1 to 11 based on player speed
            GenericPlatformHit(go, collision);
            m_rbody.velocity += m_rbody.velocity.normalized * (11.0f - m_rbody.velocity.magnitude * 0.1f);
        } else if (go.CompareTag("platform_reflect")) {
            // Reflect bounce
            GenericPlatformHit(go, collision);
            go.SendMessage("Reflect", m_rbody);
        } else if (go.CompareTag("platform_slowmo")) {
            // Bullet time bounce
            GenericPlatformHit(go, collision);
            if (!m_bullet_time)
            { 
                Time.timeScale = 0.02f;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                m_bullet_speed = m_rbody.velocity.magnitude;
                Invoke("RevokeBulletTime", 0.032f);
            }
        } else if (go.CompareTag("platform_stop")) {
            // Stop bounce
            GenericPlatformHit(go, collision);
            // Less annoying than completely stopping
            m_rbody.velocity = Vector3.ClampMagnitude(m_rbody.velocity, 5.0f);
        } else if (go.CompareTag("barrier")) {
            // Barrier collision
            go.SendMessage("ReceiveDamage", m_rbody);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        GameObject go = other.gameObject;
        if (go.CompareTag("level_edge"))
        {
            // Player has died
            GameManager.instance.LevelFailed();
        } else if (go.CompareTag("trigger_area")) {
            // Trigger the trigger
            go.SendMessage("ExecuteTrigger");
        } else if (go.CompareTag("power_up")) { 
            // Update ability
            go.SendMessage("UpgradePlayerAbility");
        }
    }

    void GenericPlatformHit(GameObject go, Collision cd)
    {
        // Note(Core): replace with new particles
        // Make the platform glow and emit hit particles
        go.SendMessage("PlaformEffects");
        Color platform_colour = go.GetComponent<PlatformProperties>().colour;
        hit_particles.startColor = platform_colour;
        hit_particles.transform.position = cd.contacts[0].point + 
                                           cd.contacts[0].normal * 0.5f;
        hit_particles.Emit(1);
        // Change the plasma ball colour to match the platform
        for (int i = 0; i < ball_particles.Length; i++)
        {
            ball_particles[i].startColor = platform_colour;
        }
        m_trail_mat.SetColor(m_trail_tint_id, platform_colour);
    }

    void RevokeBulletTime()
    {
        // Disable bullet time
        m_bullet_time = false;
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        // Boost the player towards their input direction
        Vector3 boost_dir = new Vector3(Input.acceleration.x, 0, Input.acceleration.y);
//#if UNITY_EDITOR || UNITY_STANDALONE // Note(me): add full controller support
        // Controller input is just used for testing (similar values)
        boost_dir.x += Input.GetAxisRaw("Horizontal");
        boost_dir.z += Input.GetAxisRaw("Vertical");
//#endif
        m_rbody.velocity = boost_dir.normalized * m_bullet_speed;
    }
}
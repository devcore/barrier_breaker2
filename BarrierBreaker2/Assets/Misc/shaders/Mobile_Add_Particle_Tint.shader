﻿// Simplified Additive Particle shader. Differences from regular Additive Particle one:
// - no Tint color
// - no Smooth particle support
// - no AlphaTest
// - no ColorMask

Shader "Mobile/Particles/Additive_Tint" 
{
    Properties
    {
        _TintColor("Tint Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _MainTex("Particle Texture", 2D) = "white" {}
    }
    Category
    {
        Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
        Blend SrcAlpha One
        Cull Off Lighting Off ZWrite Off Fog{ Mode Off }
        Offset -1, -1

        BindChannels
        {
            Bind "Color", color
            Bind "Vertex", vertex
            Bind "TexCoord", texcoord
        }
        SubShader
        {
            Pass
            {
                SetTexture[_MainTex] 
                {
                    constantColor[_TintColor]
                    combine texture * primary
                    combine texture * constant
                }
            }
        }
    }
}
## Barrier Breaker 2: Privacy policy

Welcome to Barrier Breaker game for Android!

This is an free unity developed application. The source code is available on BitBucket; the app is also available on Google Play.

I have not programmed to collect any data. High score is stored on the device only and is erased with the app's data or by deleting the whole application.

If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protectes your privacy, please send me an email and I will surely try to fix it/help you.

Yours sincerely,  

PseudoKor
pseudo.kor@gmail.com